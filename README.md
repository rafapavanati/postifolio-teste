# Read Me First

## SWAGGER ``http://localhost:8080/swagger-ui/index.html``

### Instruções de como executar o projeto

```mvn spring-boot:run```

### Realizando login de usuario

Realizar chamada POST para o endpoint ``http://localhost:8080/api/auth/login`` passando parametros de login e
password

Usuario pré cadastrados na base

* perfil admin :``{
  "login": "admin2",
  "password":"admin2"
  }``
* perfil user :``{
  "login": "user",
  "password":"user"
  }``

Sendo possivel o cadastro de novos usuario pelo endpoint ``http://localhost:8080/api/auth/register``

### Chamadas HTTP

Todas as chamadas HTTP dever ter o Header ``Authorization: Bearer {$TOKEN}`` 

Todos os endpoints com o verbo POST com exceção do cadastro de usuario necessita perfil de ADMIN

### Sobre o projeto 
O projeto se trata de um cadastro de pessoas e projetos, aonde podemos vincular pessoas aos projetos e movimentar o projeto, existindo algumas regras como: 
* Todos os usuario vinculados ao projeto deve ser funcionario
* Projeto já iniciado não pode ser excluido



 