CREATE TABLE usuario
( id bigserial NOT NULL,
  login character varying(100) NOT NULL UNIQUE,
  senha character varying(255) NOT NULL,
  admin boolean,
 PRIMARY KEY (id)
 );
