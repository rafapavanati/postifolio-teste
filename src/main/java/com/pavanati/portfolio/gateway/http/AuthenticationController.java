package com.pavanati.portfolio.gateway.http;

import com.pavanati.portfolio.domain.model.Usuario;
import com.pavanati.portfolio.gateway.model.request.AuthenticationRequest;
import com.pavanati.portfolio.gateway.model.request.UsuarioRequest;
import com.pavanati.portfolio.gateway.model.response.AuthenticationResponse;
import com.pavanati.portfolio.infra.security.TokenService;
import com.pavanati.portfolio.service.usuario.UsuarioService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
@Tag(name = "Auth", description = "API de autenticação")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @PostMapping("/register")
    @Operation(summary = "Registra um novo usuario")
    public ResponseEntity<?> register(@RequestBody UsuarioRequest request) {
        usuarioService.cadastrarUsuario(Usuario.builder()
                .login(request.getLogin())
                .senha(bCryptPasswordEncoder.encode(request.getPassword()))
                .admin(request.getAdmin())
                .build());
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/login")
    @Operation(summary = "Autentica usuario")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request) {
        var authenticationToken = new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword());
        var authentication = authenticationManager.authenticate(authenticationToken);
        var tokenJWT = tokenService.gerarToken((Usuario) authentication.getPrincipal());

        var authenticationResponse = AuthenticationResponse.builder()
                .accessToken(tokenJWT)
                .build();
        return ResponseEntity.ok(authenticationResponse);
    }


}
