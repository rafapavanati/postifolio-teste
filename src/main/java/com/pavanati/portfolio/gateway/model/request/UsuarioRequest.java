package com.pavanati.portfolio.gateway.model.request;

import lombok.*;
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioRequest {


    private String login;

    private String password;
    @Builder.Default
    private Boolean admin = false;
}
