package com.pavanati.portfolio.gateway.http;

import com.pavanati.portfolio.domain.enums.ProjetoStatus;
import com.pavanati.portfolio.domain.model.Pessoa;
import com.pavanati.portfolio.domain.model.Projeto;
import com.pavanati.portfolio.domain.repository.PessoaRepository;
import com.pavanati.portfolio.domain.repository.ProjetoRepository;
import com.pavanati.portfolio.gateway.mapper.ProjetoMapper;
import com.pavanati.portfolio.service.projeto.ProjetoService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;

import java.time.LocalDate;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProjetoControllerIntegrationTest {

    @LocalServerPort
    private Integer port;

    @Autowired
    private ProjetoService projetoService;

    @Autowired
    private ProjetoMapper projetoMapper;

    @Autowired
    private ProjetoRepository projetoRepository;

    @Autowired
    private PessoaRepository pessoaRepository;


    private static final String URL = "/api/projetos";

    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(
            "postgres:15-alpine"
    );

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @BeforeAll
    static void beforeAll() {
        postgres.start();
    }

    @AfterAll
    static void afterAll() {
        postgres.stop();
    }


    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgres::getJdbcUrl);
        registry.add("spring.datasource.username", postgres::getUsername);
        registry.add("spring.datasource.password", postgres::getPassword);
    }

    public String invalidRootMock = "\n" +
            "    {\n" +
            "  \"id\": 0,\n" +
            "  \"nome\": \"string\",\n" +
            "  \"dataInicio\": \"2023-12-29\",\n" +
            "  \"dataPrevisaoFim\": \"2023-12-29\",\n" +
            "  \"dataFim\": \"2023-12-29\",\n" +
            "  \"descricao\": \"string\",\n" +
            "  \"status\": \"EM_ANALISE\",\n" +
            "  \"risco\": \"BAIXO\",\n" +
            "  \"orcamento\": 0,\n" +
            "  \"gerente\": {\n" +
            "    \"id\": 1,\n" +
            "    \"nome\": \"John\",\n" +
            "    \"dataNascimento\": \"2023-12-29\",\n" +
            "    \"cpf\": \"123\",\n" +
            "    \"funcionario\": false\n" +
            "  },\n" +
            "  \"membros\": [\n" +
            "    {\n" +
            "      \"id\": 1,\n" +
            "      \"nome\": \"John\",\n" +
            "      \"dataNascimento\": \"2023-12-29\",\n" +
            "      \"cpf\": \"123\",\n" +
            "      \"funcionario\": false\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public String validRootMock = "\n" +
            "    {\n" +
            "  \"id\": 0,\n" +
            "  \"nome\": \"string\",\n" +
            "  \"dataInicio\": \"2023-12-29\",\n" +
            "  \"dataPrevisaoFim\": \"2023-12-29\",\n" +
            "  \"dataFim\": \"2023-12-29\",\n" +
            "  \"descricao\": \"string\",\n" +
            "  \"status\": \"EM_ANALISE\",\n" +
            "  \"risco\": \"BAIXO\",\n" +
            "  \"orcamento\": 0,\n" +
            "  \"gerente\": {\n" +
            "    \"id\": 1,\n" +
            "    \"nome\": \"John\",\n" +
            "    \"dataNascimento\": \"2023-12-29\",\n" +
            "    \"cpf\": \"123\",\n" +
            "    \"funcionario\": true\n" +
            "  },\n" +
            "  \"membros\": [\n" +
            "    {\n" +
            "      \"id\": 1,\n" +
            "      \"nome\": \"John\",\n" +
            "      \"dataNascimento\": \"2023-12-29\",\n" +
            "      \"cpf\": \"123\",\n" +
            "      \"funcionario\": true\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    @Test
    public void deveCadastrarProjeto() {
        List<Pessoa> pessoas = List.of(
                new Pessoa(1L, "John", LocalDate.now(), "123", true)
        );
        pessoaRepository.saveAll(pessoas);
        given()
                .contentType(ContentType.JSON)
                .when()
                .body(validRootMock.getBytes())
                .post(URL)
                .then()
                .statusCode(200);
        projetoRepository.deleteAll();
    }

    @Test
    public void naoDeveCadastrarProjetoComPessoasNaoFuncionario() {
        List<Pessoa> pessoas = List.of(
                new Pessoa(1L, "John", LocalDate.now(), "123", false)
        );
        pessoaRepository.saveAll(pessoas);
        given()
                .contentType(ContentType.JSON)
                .when()
                .body(invalidRootMock.getBytes())
                .post(URL)
                .then()
                .statusCode(500);
        projetoRepository.deleteAll();
    }

    @Test
    public void naoDevePermitirRemoverProjetoEmAndamento() {
        Pessoa gerente = new Pessoa(1L, "John", LocalDate.now(), "123", true);
        List<Pessoa> pessoas = List.of(
                gerente
        );
        pessoaRepository.saveAll(pessoas);

        Projeto projeto = projetoRepository.save(Projeto.builder().nome("teste").status(ProjetoStatus.EM_ANDAMENTO).gerente(gerente).membros(pessoas).id(1L).build());
        given()
                .contentType(ContentType.JSON)
                .when()
                .body(validRootMock.getBytes())
                .delete(URL.concat("/%d".formatted(projeto.getId())))
                .then()
                .statusCode(500);
        projetoRepository.deleteAll();
    }

    @Test
    public void deveAtualizarProjeto() {
        Pessoa gerente = new Pessoa(1L, "John", LocalDate.now(), "123", true);
        List<Pessoa> pessoas = List.of(
                gerente
        );
        pessoaRepository.saveAll(pessoas);
        Projeto projeto = projetoRepository.save(Projeto.builder().nome("teste").gerente(gerente).membros(pessoas).id(1L).build());

        given()
                .contentType(ContentType.JSON)
                .when()
                .body(validRootMock.getBytes())
                .put(URL.concat("/%d".formatted(projeto.getId())))
                .then()
                .statusCode(200);

        projetoRepository.deleteAll();
    }

    @Test
    public void deveBuscaProjeto() {
        Pessoa gerente = new Pessoa(1L, "John", LocalDate.now(), "123", true);
        List<Pessoa> pessoas = List.of(
                gerente
        );
        pessoaRepository.saveAll(pessoas);
        Projeto projeto = projetoRepository.save(Projeto.builder().nome("teste").gerente(gerente).membros(pessoas).id(1L).build());

        given()
                .contentType(ContentType.JSON)
                .when()
                .body(validRootMock.getBytes())
                .get(URL.concat("/%d".formatted(projeto.getId())))
                .then()
                .statusCode(200);
        projetoRepository.deleteAll();
    }

    @Test
    public void deveRemoverProjeto() {
        Pessoa gerente = new Pessoa(1L, "John", LocalDate.now(), "123", true);
        List<Pessoa> pessoas = List.of(
                gerente
        );
        pessoaRepository.saveAll(pessoas);

        Projeto projeto = projetoRepository.save(Projeto.builder().nome("teste").gerente(gerente).membros(pessoas).id(1L).build());
        given()
                .contentType(ContentType.JSON)
                .when()
                .body(validRootMock.getBytes())
                .delete(URL.concat("/%d".formatted(projeto.getId())))
                .then()
                .statusCode(204);
        projetoRepository.deleteAll();
    }

    @Test
    public void deveListarProjetos() {
        Pessoa gerente = new Pessoa(1L, "John", LocalDate.now(), "123", true);
        List<Pessoa> pessoas = List.of(
                gerente
        );
        pessoaRepository.saveAll(pessoas);
        List<Projeto> projetos = List.of(
                Projeto.builder().nome("teste").gerente(gerente).membros(pessoas).build(),
                Projeto.builder().nome("teste").gerente(gerente).membros(pessoas).build()
        );
        projetoRepository.saveAll(projetos);
        given()
                .contentType(ContentType.JSON)
                .when()
                .get(URL)
                .then()
                .body(".", hasSize(2));
        projetoRepository.deleteAll();
    }

}
