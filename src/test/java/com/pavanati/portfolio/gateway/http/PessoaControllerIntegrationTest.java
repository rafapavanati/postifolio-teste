package com.pavanati.portfolio.gateway.http;

import com.pavanati.portfolio.domain.model.Pessoa;
import com.pavanati.portfolio.domain.repository.PessoaRepository;
import com.pavanati.portfolio.gateway.mapper.PessoaMapper;
import com.pavanati.portfolio.service.pessoa.PessoaService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;

import java.time.LocalDate;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasSize;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PessoaControllerIntegrationTest {

    @LocalServerPort
    private Integer port;

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private PessoaMapper pessoaMapper;

    private static final String URL = "/api/pessoas";

    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(
            "postgres:15-alpine"
    );

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @BeforeAll
    static void beforeAll() {
        postgres.start();
    }

    @AfterAll
    static void afterAll() {
        postgres.stop();
    }


    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgres::getJdbcUrl);
        registry.add("spring.datasource.username", postgres::getUsername);
        registry.add("spring.datasource.password", postgres::getPassword);
    }


    public String validRootMock = "\n" +
            "    {\n" +
            "        \n" +
            "        \"id\": \"1\",\n" +
            "        \"nome\": \"TESTE\",\n" +
            "        \"dataNascimento\": \"1993-06-16\",\n" +
            "        \"cpf\": \"123456\",\n" +
            "        \"funcionario\": false\n" +
            "    }";

    @Test
    public void deveCadastrarPessoa() {
        given()
                .contentType(ContentType.JSON)
                .when()
                .body(validRootMock.getBytes())
                .post(URL)
                .then()
                .statusCode(200);
    }

    @Test
    public void deveAtualizarPessoa() {
        Pessoa pessoa = pessoaRepository.save(new Pessoa(1L, "John", LocalDate.now(), "123", false));

        given()
                .contentType(ContentType.JSON)
                .when()
                .body(validRootMock.getBytes())
                .put(URL.concat("/%d".formatted(pessoa.getId())))
                .then()
                .statusCode(200);
    }

    @Test
    public void deveBuscaPessoa() {
        Pessoa pessoa = pessoaRepository.save(new Pessoa(1L, "John", LocalDate.now(), "123", false));

        given()
                .contentType(ContentType.JSON)
                .when()
                .body(validRootMock.getBytes())
                .delete(URL.concat("/%d".formatted(pessoa.getId())))
                .then()
                .statusCode(204);
    }

    @Test
    public void deveRemoverPessoa() {
        Pessoa pessoa = pessoaRepository.save(new Pessoa(1L, "John", LocalDate.now(), "123", false));
        given()
                .contentType(ContentType.JSON)
                .when()
                .body(validRootMock.getBytes())
                .delete(URL.concat("/%d".formatted(pessoa.getId())))
                .then()
                .statusCode(204);
    }

    @Test
    public void deveListarPessoas() {
        List<Pessoa> pessoas = List.of(
                new Pessoa(null, "John", LocalDate.now(), "123", false),
                new Pessoa(null, "Dennis", LocalDate.now(), "123", true)
        );
        pessoaRepository.saveAll(pessoas);
        given()
                .contentType(ContentType.JSON)
                .when()
                .get(URL)
                .then()
                .body(".", hasSize(2));
    }

}
